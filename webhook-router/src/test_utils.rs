// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::env;
use std::fs::File;
use std::path::{Path, PathBuf};

use log::{Level, LevelFilter, Log, Metadata, Record};
use serde_json::Value;
use tempfile::TempDir;

pub fn setup_logging() {
    struct SimpleLogger;

    impl Log for SimpleLogger {
        fn enabled(&self, metadata: &Metadata) -> bool {
            metadata.level() <= Level::Debug
        }

        fn log(&self, record: &Record) {
            if self.enabled(record.metadata()) {
                println!("[{}] {}", record.level(), record.args());
            }
        }

        fn flush(&self) {}
    }

    static LOGGER: SimpleLogger = SimpleLogger;

    // Since the tests run in parallel, this may get called multiple times. Just ignore errors.
    let _ = log::set_logger(&LOGGER);
    log::set_max_level(LevelFilter::Debug);
}

pub fn create_tempdir() -> TempDir {
    setup_logging();

    let mut working_dir = env::current_exe().unwrap();
    working_dir.pop();

    TempDir::new_in(working_dir).unwrap()
}

pub fn write_config(path: &Path, config: Value) -> PathBuf {
    let config_path = path.join("config.json");
    let mut fout = File::create(&config_path).unwrap();
    serde_json::to_writer(&mut fout, &config).unwrap();
    config_path
}

pub fn write_config_secrets(path: &Path, mut config: Value, secrets: Value) -> (PathBuf, PathBuf) {
    let secrets_path = path.join("secrets.json");

    config
        .as_object_mut()
        .unwrap()
        .insert("secrets".into(), secrets_path.to_str().unwrap().into());

    let config_path = write_config(path, config);

    let mut fout = File::create(&secrets_path).unwrap();
    serde_json::to_writer(&mut fout, &secrets).unwrap();
    (config_path, secrets_path)
}
