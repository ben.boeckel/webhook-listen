// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

// XXX(rust-1.66)]
#![allow(clippy::uninlined_format_args)]

//! Webhook listener
//!
//! This program listens over HTTP for JSON webhook events to pass along to a handler.
//!
//! See the [usage](usage.html) documentation for more.

#![warn(missing_docs)]

use std::convert::Infallible;
use std::error::Error;
use std::net::ToSocketAddrs;
use std::path::Path;
use std::sync::Arc;

use clap::{App, Arg, ArgAction};
use hyper::client::Client;
use hyper::server::Server;
use hyper::{service, Body, Request};
use log::LevelFilter;
#[cfg(feature = "systemd")]
use systemd::journal::JournalLog;
use thiserror::Error;
use webhook_router::{Config, Router};

/// Errors which can occur when handling alternative commands.
#[derive(Debug, Error)]
pub enum ServiceError {
    /// Hyper had a problem.
    #[error("hyper error: {}", _0)]
    Hyper(#[from] hyper::Error),
    /// The other end returned an error.
    #[error("http error: {}", _0)]
    Http(#[from] http::Error),
    /// Multiple addresses were given.
    #[error("multiple socket addresses")]
    MultipleSocketAddrs,
    /// No addresses were given.
    #[error("no socket addresses")]
    NoSocketAddrs,
}

/// Run on an address based on a configuration at the given path.
async fn run_from_config(address: &str, config_path: &Path) -> Result<(), Box<dyn Error>> {
    let router = Arc::new(Router::new(config_path)?);
    let make_service = service::make_service_fn(move |_| {
        let router = router.clone();

        async move {
            Ok::<_, Infallible>(service::service_fn(move |req: Request<Body>| {
                let router = router.clone();

                async move {
                    let (parts, body) = req.into_parts();
                    let bytes = hyper::body::to_bytes(body).await?.into_iter().collect();
                    let req = Request::from_parts(parts, bytes);
                    router
                        .handle(&req)
                        .map(|rsp| rsp.map(Body::from))
                        .map_err(ServiceError::from)
                }
            }))
        }
    });

    let socket_addrs = address.to_socket_addrs()?.collect::<Vec<_>>();
    if socket_addrs.len() > 1 {
        return Err(ServiceError::MultipleSocketAddrs.into());
    }
    let socket_addr = socket_addrs
        .into_iter()
        .next()
        .ok_or(ServiceError::NoSocketAddrs)?;
    Server::bind(&socket_addr).serve(make_service).await?;

    Ok(())
}

#[derive(Debug, Error)]
enum LogError {
    #[cfg(feature = "sentry")]
    #[error("`sentry` requires `--sentry-endpoint=`")]
    SentryEndpointMissing,
    #[cfg(not(feature = "sentry"))]
    #[error("logging: `sentry` support is not available")]
    NoSentry,
    #[cfg(not(feature = "systemd"))]
    #[error("logging: `systemd` support is not available")]
    NoSystemd,

    #[error("unknown logger: {}", _0)]
    UnknownLogger(String),
}

enum Logger {
    #[cfg(feature = "systemd")]
    Systemd,
    #[cfg(feature = "sentry")]
    Sentry(sentry::ClientInitGuard),
    Env,
}

#[derive(Debug, Error)]
enum RunError {
    #[error("tokio startup: {}", source)]
    TokioStartup {
        #[source]
        source: std::io::Error,
    },
    #[error("reload HTTP error: {}", source)]
    ReloadHttp {
        #[source]
        source: hyper::Error,
    },
    #[error("reload extraction: {}", source)]
    ReloadExtraction {
        #[source]
        source: hyper::Error,
    },
    #[error("reload error: {}", reason)]
    Reload { reason: String },
}

impl RunError {
    fn tokio_startup(source: std::io::Error) -> Self {
        Self::TokioStartup {
            source,
        }
    }

    fn reload_http(source: hyper::Error) -> Self {
        Self::ReloadHttp {
            source,
        }
    }

    fn reload_extraction(source: hyper::Error) -> Self {
        Self::ReloadExtraction {
            source,
        }
    }

    fn reload(reason: &[u8]) -> Self {
        Self::Reload {
            reason: String::from_utf8_lossy(reason).into(),
        }
    }
}

/// A `main` function which supports `try!`.
fn try_main() -> Result<(), Box<dyn Error>> {
    human_panic::setup_panic!();

    let matches = App::new("webhook-listen")
        .version(clap::crate_version!())
        .author("Ben Boeckel <ben.boeckel@kitware.com>")
        .about("Listen over HTTP for JSON webhook events to pass along to a handler")
        .arg(
            Arg::with_name("ADDRESS")
                .short('a')
                .long("address")
                .help("The address to listen on")
                .required_unless("VERIFY")
                .value_name("ADDRESS")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("CONFIG")
                .short('c')
                .long("config")
                .help("Path to the configuration file")
                .required_unless("RELOAD")
                .value_name("FILE")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("DEBUG")
                .short('d')
                .long("debug")
                .help("Increase verbosity")
                .action(ArgAction::Count),
        )
        .arg(
            Arg::with_name("LOGGER")
                .short('l')
                .long("logger")
                .default_value("env")
                .possible_values([
                    "env",
                    #[cfg(feature = "systemd")]
                    "systemd",
                    #[cfg(feature = "sentry")]
                    "sentry",
                ])
                .help("Logging backend")
                .value_name("LOGGER")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("SENTRY_ENDPOINT")
                .long("sentry-endpoint")
                .help("Sentry logging endpoint")
                .hidden(cfg!(not(feature = "sentry")))
                .value_name("ENDPOINT")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("VERIFY")
                .short('v')
                .long("verify")
                .help("Check the configuration file and exit"),
        )
        .arg(
            Arg::with_name("RELOAD")
                .short('r')
                .long("reload")
                .help("Reload the configuration"),
        )
        .get_matches();

    let log_level = match matches.get_one::<u8>("DEBUG").copied().unwrap_or(0) {
        0 => LevelFilter::Error,
        1 => LevelFilter::Warn,
        2 => LevelFilter::Info,
        3 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };

    let _logger = match matches
        .get_one::<String>("LOGGER")
        .expect("logger should have a value")
        .as_ref()
    {
        "env" => {
            env_logger::Builder::new().filter(None, log_level).init();
            Logger::Env
        },

        #[cfg(feature = "systemd")]
        "systemd" => {
            JournalLog::init()?;
            Logger::Systemd
        },
        #[cfg(not(feature = "systemd"))]
        "systemd" => {
            return Err(LogError::NoSystemd.into());
        },

        #[cfg(feature = "sentry")]
        "sentry" => {
            if let Some(endpoint) = matches.value_of("SENTRY_ENDPOINT") {
                Logger::Sentry(sentry::init(endpoint))
            } else {
                return Err(LogError::SentryEndpointMissing.into());
            }
        },
        #[cfg(not(feature = "sentry"))]
        "sentry" => {
            return Err(LogError::NoSentry.into());
        },

        logger => {
            return Err(LogError::UnknownLogger(logger.into()).into());
        },
    };

    log::set_max_level(log_level);

    let config_path = Path::new(
        matches
            .get_one::<String>("CONFIG")
            .expect("the configuration option is required"),
    );

    if matches.is_present("VERIFY") {
        Config::from_path(config_path)?;
    } else if matches.is_present("RELOAD") {
        let address = matches
            .get_one::<String>("ADDRESS")
            .expect("the address option is required");
        let url = format!("http://{}/__reload", address);
        let req = Request::put(url).body(Body::empty())?;
        let client = Client::new();

        let rt = tokio::runtime::Runtime::new().map_err(RunError::tokio_startup)?;
        rt.block_on(async {
            let rsp = client.request(req).await.map_err(RunError::reload_http)?;
            if rsp.status().is_success() {
                Ok(())
            } else {
                let bytes = hyper::body::to_bytes(rsp.into_body())
                    .await
                    .map_err(RunError::reload_extraction)?
                    .into_iter()
                    .collect::<Vec<_>>();
                Err(RunError::reload(&bytes))
            }
        })?;
    } else {
        let rt = tokio::runtime::Runtime::new().map_err(RunError::tokio_startup)?;
        rt.block_on(run_from_config(
            matches
                .get_one::<String>("ADDRESS")
                .expect("the address option is required"),
            config_path,
        ))?;
    }

    Ok(())
}

/// The entry point.
///
/// Wraps around `try_main` to panic on errors.
fn main() {
    if let Err(err) = try_main() {
        panic!("{:?}", err);
    }
}
